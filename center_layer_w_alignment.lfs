{ Max Kellermeier }
{ 24.05.2018 }
{ Making a layer of rods including the covering wall to cut out the structure and adding a wedge at one side for the alignment pin. File is based on 'three_rods_faces_bulk_sampled_between_rects.lfs' and 'alignment_v_walls.lfs' }
Pcor False;
Speed 8000;
Pol 90;

@thickness, layer_num, n_layers, n_cells, freq_rel, layer_height, radius_rel, defect_rel, lattice_const, radius, length, a, b, angular_resolution, distance_in_layer, i, j, k, padding, num_rods;
@radius_defect;
@n_steps_in_layer, n_steps_in_layer, step_with_x;
@len_bulk_detach, angular_res_bulk;
@box_height, box_width, box_z_step, box_x_step;

padding:=50;

{ Variables }
thickness:= 230;
layer_num:= 0;		{ranges from -4 to 4}
n_cells:= 4;	{unused up to now}
n_layers:=9;    {n_layers = n_cells*2 +1}
num_rods := n_layers-ABS(layer_num);
freq_rel:= 0.52;  { defect mode frequency in relative units}
layer_height:= SQRT(3)/2;  {in units of the lattice constant}
radius_rel:= 0.38; { in units of the lattice constant }
defect_rel:= 0.97;

lattice_const:= thickness/layer_height;
radius:= radius_rel*lattice_const;
radius_defect := defect_rel*lattice_const;

length:= 5000;
a:= 1;
b:= 10;
angular_resolution:= 180;

distance_in_layer:= lattice_const - 2*radius - 2*a;
n_steps_in_layer:=100;
step_with_x:= distance_in_layer/n_steps_in_layer;

{Bulk etching variables}
len_bulk_detach:=5;
angular_res_bulk:=40;

{Boxes for bulk below and above the cylinders}
box_height := 56;
box_width := lattice_const*num_rods + distance_in_layer;
box_z_step := 8;
box_x_step := 1;

{Distance between the cylinders: 290um -> width in x: 2034um}
{covering wall: 12000um x 5300um}
@padding_wall;
padding_wall := 120;
@wall_height, wall_width_x, wall_width_y, step_z, num_z_wall;
wall_height:= thickness + 2*padding_wall;
wall_width_x:=15000;
wall_width_y:=5300;
step_z:=8;
num_z_wall:= wall_height/step_z;

{Glueing raster}
@num_glue_lines, glue_lines_dx, glue_lines_margin, num_z_glue_lines, glue_lines_height;

num_glue_lines:=5;
glue_lines_margin:= 2500;
glue_lines_dx:= 50;
glue_lines_height :=100;
num_z_glue_lines:= INT((glue_lines_height+padding)/step_z);                                                                         

{--- Main Code starts here ---}
{Initially placed at top}
{bulk_etching_between_bottom_half_cylinders( length, lattice_const, radius, halfaxis_a, halfaxis_b, angular_resolution, sign_x_dir, location : AnsiString);} 
{Mark the center point along x axis; at surface}

definehome;
{move to the side such that the structure is written symmetrically around the z-axis}
mrel (-lattice_const*(num_rods - 1)/2), 0,0;

mrel 0,0, (-thickness/2 -radius-b);




{---Walls at bottom ---}
mrel (-radius-a - distance_in_layer), 0, (-box_height);
Loop k, 0, (box_height/box_z_step);
    Line box_width, 0, 0;
    Line 0, (length + len_bulk_detach), 0;
    Line (-box_width), 0, 0;
    Line 0, -(length + len_bulk_detach), 0; 
    mrel 0, 0, box_z_step;
EndLoop;

{horizontal plane for cutting out the box}
Pol 90;
Loop k, 0, (box_width/box_x_step);
    Line 0, (length + len_bulk_detach), 0;
    Line 0, -(length + len_bulk_detach), 0;
    mrel box_x_step, 0, 0;
EndLoop;
mrel (-box_width), 0, 0;

mrel (radius+a+distance_in_layer), 0, 0;




{---First 3 cylinders---}						
{- Bulk sampling for first cylinder -}
mrel (-lattice_const + radius ),0,0;

{- Sampling of the bulk glass to detach at the cylinder top and bottom -}
{4 Walls, two at the top end of the cylinder, two at the bottom end}
mrel 0,0, (-thickness/2 + radius + b - padding);
Pol 90;
{Height of the wall is 350, width is distance_in_layer}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

{second wall at the other end of the bulk sampling along y} 
mrel 0, len_bulk_detach, 0;
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, length,0;
{third wall at bottom end}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, -len_bulk_detach, 0;
{fourth wall at bottom end}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, -length, 0;


{Move back to cylinder, focused on the center along z, not on the bottom}
mrel 0,0, (thickness/2 + padding);

{ horizontal plane  between two rods. ... length, distance_in_layer, step_with_x, 0, 0; }
Loop i, 0, n_steps_in_layer
    Line 0, length, 0;
    Line 0, (-length), 0;
    mrel step_with_x, 0, 0;
EndLoop
Line 0, length, 0;
Line 0, (-length), 0;

{Additional wall along y between the cylinders}
mrel (-distance_in_layer/2), 0, (-thickness/2 - padding);
Pol 0;
Loop i,0, 35;
    Line 0, length+len_bulk_detach,0;
    Line 0, (-length-len_bulk_detach),0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);
mrel (distance_in_layer/2), 0, (thickness/2 + padding);

mrel (radius+a), 0 , (-radius-b);                           
Loop k, 0, 3

    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
    mrel 0, length,0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
    mrel 0, (len_bulk_detach/2 - length),0;

    PERPCYLINDER length, radius, a,b, angular_resolution;

    {- Vertical planes to top surface -}
    mrel 0,0, 2*(radius+b);
    Loop i,0,10;
        Line 0, length, 0;
        Line 0, -length, 0;
        mrel 0,0, 10;
    EndLoop
    mrel 0, 0, -100;
    mrel 0,0, (-2*(radius+b));

    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    mrel 0, (-len_bulk_detach/2), 0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
    mrel 0, length,0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
    mrel 0, -length,0;

    mrel (radius+a), 0, 0;

    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    {4 Walls, two at the top end of the cylinder, two at the bottom end}
    mrel 0,0, (-thickness/2 + radius + b - padding);
    Pol 90;
    {Height of the wall is 350, width is distance_in_layer}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    {second wall at the other end of the bulk sampling along y} 
    mrel 0, len_bulk_detach, 0;
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, length,0;
    {third wall at bottom end}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, -len_bulk_detach, 0;
    {fourth wall at bottom end}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, -length, 0;


    {Move back to cylinder, focused on the center along z, not on the bottom}
    mrel 0,0, (thickness/2 + padding);

    { horizontal plane  between two rods. ... length, distance_in_layer, step_with_x, 0, 0; }
    Loop i, 0, n_steps_in_layer
        Line 0, length, 0;
        Line 0, (-length), 0;
        mrel step_with_x, 0, 0;
    EndLoop
    Line 0, length, 0;
    Line 0, (-length), 0;

    {Additional wall along y between the cylinders}
    mrel (-distance_in_layer/2), 0, (-thickness/2 - padding);
    Pol 0;
    Loop i,0, 35;
        Line 0, length+len_bulk_detach,0;
        Line 0, (-length-len_bulk_detach),0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);
    mrel (distance_in_layer/2), 0, (thickness/2 + padding);

    mrel (radius+a), 0 , (-radius-b);

EndLoop



{--- Hollow core rods ---}
{- Left cut rod -}
{- Sampling of the bulk glass to detach at the cylinder top and bottom -}
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
mrel 0, length,0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
mrel 0, (len_bulk_detach/2 - length),0;

{ PERPCYLINDER length, radius, a,b, angular_resolution; }
{ cut_cylinder(      length, radius, halfaxis_a, halfaxis_b,     cut_radius, cut_distance, relative_angle,         angular_res : AnsiString);  }
CUTCYLINDER length, radius, a, b, radius_defect, lattice_const, 0, angular_resolution;

{- Vertical planes to top surface -}
mrel 0,0, 2*(radius+b);
Loop i,0,10;
    Line 0, length, 0;
    Line 0, -length, 0;
    mrel 0,0, 10;
EndLoop
mrel 0, 0, -100;
mrel 0,0, (-2*(radius+b));

{- Sampling of the bulk glass to detach at the cylinder top and bottom -}
mrel 0, (-len_bulk_detach/2), 0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
mrel 0, length,0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
mrel 0, -length,0;

mrel (radius+a), 0, 0;


{- Core -}
mrel 0,0, (-thickness/2 + radius + b - padding);
mrel -150,0,0;
{Height of the wall is 350, width is distance_in_layer}
Loop i,0, 35
    Pol 90;
    Line lattice_const+2*distance_in_layer,0,0;
    Pol 0;
    Line 0, length+len_bulk_detach+300, 0;
    Pol 90;
    Line -(lattice_const+2*distance_in_layer),0,0;
    Pol 0;
    Line 0, -(length+len_bulk_detach+300 ), 0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);
Pol 90;
mrel 150,0,0;
mrel 0,0, (thickness/2 + padding - radius -b);


{- Right cut rod -}
{1. Move to the position at the opposite end along y}
{2. Rotate the axis by 180 deg around z}
{3. Write the cut cylinder}
{4. Rotate back}
{5. Move back}

{1.}
mrel (lattice_const+distance_in_layer + radius+a), 0,0;
mrel 0, length+len_bulk_detach, 0;
{2. }
RotateAxis 0, 0, 180;
{3. }
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
mrel 0, length,0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
mrel 0, (len_bulk_detach/2 - length),0;

{ PERPCYLINDER length, radius, a,b, angular_resolution; }
{ cut_cylinder(      length, radius, halfaxis_a, halfaxis_b,     cut_radius, cut_distance, relative_angle,         angular_res : AnsiString);  }
CUTCYLINDER length, radius, a, b, radius_defect, lattice_const, 0, angular_resolution;

{- Vertical planes to top surface -}
mrel 0,0, 2*(radius+b);
Loop i,0,10;
    Line 0, length, 0;
    Line 0, -length, 0;
    mrel 0,0, 10;
EndLoop
mrel 0, 0, -100;
mrel 0,0, (-2*(radius+b));

{- Sampling of the bulk glass to detach at the cylinder top and bottom -}
mrel 0, (-len_bulk_detach/2), 0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
mrel 0, length,0;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
mrel 0, -length,0;

{4.}
RotateAxis 0, 0, 0;
{5.}
mrel 0, -(length+len_bulk_detach), 0;
{section end}




{--- Second 3 rods ---}
Loop k, 0, 3
    mrel radius+a, 0,0;
    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    {4 Walls, two at the top end of the cylinder, two at the bottom end}
    mrel 0,0, (-thickness/2 + radius + b - padding);
    Pol 90;
    {Height of the wall is 350, width is distance_in_layer}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    {second wall at the other end of the bulk sampling along y} 
    mrel 0, len_bulk_detach, 0;
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, length,0;
    {third wall at bottom end}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, -len_bulk_detach, 0;
    {fourth wall at bottom end}
    Loop i,0, 35
        Line distance_in_layer,0,0;
        Line -distance_in_layer,0,0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);

    mrel 0, -length, 0;


    {Move back to cylinder, focused on the center along z, not on the bottom}
    mrel 0,0, (thickness/2 + padding);

    { horizontal plane  between two rods. ... length, distance_in_layer, step_with_x, 0, 0; }
    Loop i, 0, n_steps_in_layer
        Line 0, length, 0;
        Line 0, (-length), 0;
        mrel step_with_x, 0, 0;
    EndLoop
    Line 0, length, 0;
    Line 0, (-length), 0;

    {Additional wall along y between the cylinders}
    mrel (-distance_in_layer/2), 0, (-thickness/2 - padding);
    Pol 0;
    Loop i,0, 35;
        Line 0, length+len_bulk_detach,0;
        Line 0, (-length-len_bulk_detach),0;
        mrel 0, 0, 10;
    EndLoop
    mrel 0, 0, (-35*10);
    mrel (distance_in_layer/2), 0, (thickness/2 + padding);

    mrel (radius+a), 0 , (-radius-b);

    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
    mrel 0, length,0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,1;
    mrel 0, (len_bulk_detach/2 - length),0;

    PERPCYLINDER length, radius, a,b, angular_resolution;

    {---Vertical planes to top surface---}
    mrel 0,0, 2*(radius+b);
    Loop i,0,10;
    Line 0, length, 0;
    Line 0, -length, 0;
    mrel 0,0, 10;
    EndLoop
    mrel 0, 0, -100;
    mrel 0,0, (-2*(radius+b));

    {- Sampling of the bulk glass to detach at the cylinder top and bottom -}
    mrel 0, (-len_bulk_detach/2), 0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
    mrel 0, length,0;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, 1,-1;
    CYL2RECTBULK len_bulk_detach, lattice_const, radius, a,b, angular_res_bulk, -1,-1;
    mrel 0, -length,0;

EndLoop;

{--- Bulk sampling at End along x direction ---}
mrel radius+a, 0,0;
{- Sampling of the bulk glass to detach at the cylinder top and bottom -}
{4 Walls, two at the top end of the cylinder, two at the bottom end}
mrel 0,0, (-thickness/2 + radius + b - padding);
Pol 90;
{Height of the wall is 350, width is distance_in_layer}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

{second wall at the other end of the bulk sampling along y} 
mrel 0, len_bulk_detach, 0;
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, length,0;
{third wall at bottom end}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, -len_bulk_detach, 0;
{fourth wall at bottom end}
Loop i,0, 35
    Line distance_in_layer,0,0;
    Line -distance_in_layer,0,0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);

mrel 0, -length, 0;


{Move back to cylinder, focused on the center along z, not on the bottom}
mrel 0,0, (thickness/2 + padding);

{ horizontal plane  between two rods. ... length, distance_in_layer, step_with_x, 0, 0; }
Loop i, 0, n_steps_in_layer
    Line 0, length, 0;
    Line 0, (-length), 0;
    mrel step_with_x, 0, 0;
EndLoop
Line 0, length, 0;
Line 0, (-length), 0;

{Additional wall along y between the cylinders}
mrel (-distance_in_layer/2), 0, (-thickness/2 - padding);
Pol 0;
Loop i,0, 35;
    Line 0, length+len_bulk_detach,0;
    Line 0, (-length-len_bulk_detach),0;
    mrel 0, 0, 10;
EndLoop
mrel 0, 0, (-35*10);
mrel (distance_in_layer/2), 0, (thickness/2 + padding);

mrel (radius+a), 0 , (-radius-b);









{ After writing the rods go back to the initial point for writing a box above.}
mrel (-lattice_const*num_rods), 0, 2*(radius+b);
mrel (-radius-b), 0, 0;
															  

{--- Box ---}
{horizontal plane for cutting out the box}
Pol 90;
Loop k, 0, (box_width/box_x_step);
    Line 0, (length + len_bulk_detach), 0;
    Line 0, -(length + len_bulk_detach), 0;
    mrel box_x_step, 0, 0;
EndLoop;
	  
mrel (-box_width), 0, 0;

{vertical walls}
Loop k, 0, (box_height/box_z_step);
    Line box_width, 0, 0;
    Line 0, (length + len_bulk_detach), 0;
    Line (-box_width), 0, 0;
    Line 0, -(length + len_bulk_detach), 0; 
    mrel 0, 0, box_z_step;
EndLoop;


{---Cut out wall---}
mabs 0,0,0;

mrel 0,-150,(-thickness - padding_wall);
mrel -wall_width_x/2,0,0; 
{Loop for Wall}
Loop i,0, num_z_wall
    Pol 90;
    Line wall_width_x, 0, 0;
    Pol 0;
    Line 0, wall_width_y,0, 0;
    Pol 90;
    Line (-wall_width_x), 0, 0;
    Pol 0;
    Line 0, (-wall_width_y),0, 0;
    mrel 0,0, step_z;
EndLoop

Pol 90;
Line wall_width_x, 0, 0;
Pol 0;
Line 0, wall_width_y,0, 0;
Pol 90;
Line (-wall_width_x), 0, 0;
Pol 0;
Line 0, (-wall_width_y),0, 0;

mrel 0, 0, -wall_height;

{---Alignment wedge---}
{Wedge for alignment. Edge of the covering wall is at x=7000}
mabs -wall_width_x/2, (wall_width_y -2000), (-thickness-padding_wall);
mrel (2000/SQRT(2)+10), 0, 0;


@num_steps, coord;
num_steps:=INT((thickness + 2*padding_wall)/step_z);
coord:=2000/SQRT(2);
Loop i, 0, num_steps;
    Line -coord, -coord, 0;
    Line coord, coord, 0;
    mrel 0,0, step_z;
EndLoop
Line -coord, -coord, 0;
Line coord, coord, 0;
mrel 0,0, (-step_z*num_steps);

Loop i, 0, num_steps;
    Line -coord, coord, 0;
    Line coord, -coord, 0;
    mrel 0,0, step_z;
EndLoop
Line -coord, coord, 0;
Line coord, -coord, 0;
mrel 0,0, (-step_z*num_steps);

                     
                                                       

{ RotateAxis 0, 0, 45;
{ Plane 2000, 0, 1, (thickness + 2*padding_wall), step_z;

{ RotateAxis 0, 0, -45;
{ Plane 2000, 0, 1, (thickness + 2*padding_wall), step_z;

mrel 0,0, thickness+padding_wall;

{ RotateAxis 0, 0, 0;


{--- Glueing grooves ---}
mabs 0,0,0;
{ TODO: make sure that the wall is placed at the right position
mrel (-wall_width_x/2 + glue_lines_margin), 0, 0;

mrel 0, 0, -glue_lines_height;
Loop j, 0, num_glue_lines;
    Loop i, 0, num_z_glue_lines;
        Line 0, length, 0;
        Line 0, -length, 0;
        mrel 0, 0, step_z;
    EndLoop
    mrel glue_lines_dx, 0, (-step_z*num_z_glue_lines);
EndLoop


mabs 0,0,0;
mrel (wall_width_x/2 - glue_lines_margin), 0, 0;
mrel 0, 0, -glue_lines_height;
Loop j, 0, num_glue_lines;
    Loop i, 0, num_z_glue_lines;
        Line 0, length, 0;
        Line 0, -length, 0;
        mrel 0, 0, step_z;
    EndLoop;
    mrel -glue_lines_dx, 0, (-step_z*num_z_glue_lines);
EndLoop;

mabs 0, 0, 0;